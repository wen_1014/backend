[spring全版本下载](https://repo.spring.io/release/org/springframework/spring/)

spring优点：

- spring是一个开源的免费的框架（容器）
- spring是一个轻量级的、非入侵式的框架
- 控制反转（IOC），面向切面编程（AOP）
- 支持事务的处理，对框架整合的支持

总结一句话：Spring就是一个轻量级的控制反转（IOC）和面向切面编程（AOP）的框架!





[Spring Framework中文文档](https://www.docs4dev.com/docs/zh/spring-framework/5.1.3.RELEASE/reference)





主要研究：

- Spring boot
  - 一个快速开发的脚手架
  - 基于SpringBoot可以快速地开发单个微服务。
  - 约定大于配置。

- Spring cloud
  - SpringCloud是基于SpringBoot实现的。



因为现在大多数公司都在使用SpringBoot进行快速开发，学习SpringBoot的前提，需要完全掌握Spring及SpringMVC，承上启下的作用