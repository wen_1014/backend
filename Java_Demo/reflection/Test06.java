package backend.Java_Demo.reflection;

public class Test06 {
    public static void main(String[] args) throws ClassNotFoundException {
        //获取系统类加载器
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println(systemClassLoader);

        //获取系统类加载器的父类加载器--->扩展类加载器
        ClassLoader parent = systemClassLoader.getParent();
        System.out.println(parent);

        //获取扩展类加载器的父类加载器--->根加载器(C/C++)
        ClassLoader parent1 = parent.getParent();
        System.out.println(parent1);

        //测试当前类是哪个加载器加载的
        ClassLoader classLoader = Class.forName("backend.Java_Demo.reflection.Test06").getClassLoader();
        System.out.println(classLoader);

        //测试JDK内置的类是谁加载的
        classLoader = Class.forName("java.lang.Object").getClassLoader();
        System.out.println(classLoader);

        //如何获得系统加载器可以加载的路径
        System.out.println(System.getProperty("java.class.path"));
        //G:\Environment\Java\JDK1.8\jre\lib\charsets.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\deploy.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\access-bridge-64.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\cldrdata.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\dnsns.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\jaccess.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\jfxrt.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\localedata.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\nashorn.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\sunec.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\sunjce_provider.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\sunmscapi.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\sunpkcs11.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\ext\zipfs.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\javaws.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\jce.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\jfr.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\jfxswt.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\jsse.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\management-agent.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\plugin.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\resources.jar;
        // G:\Environment\Java\JDK1.8\jre\lib\rt.jar;
        // C:\Users\jiwen&77\Desktop\Note\javaSE\out\production\java基础语法;G:\IDEA 2019.3.3\lib\idea_rt.jar

        //双亲委派机制
            //不能定义java.lang.String，因为根加载器中已存在这个jar包，不会运行用户定义的同名jar包。

    }
}
