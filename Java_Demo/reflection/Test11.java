package backend.Java_Demo.reflection;

import java.lang.annotation.*;
import java.lang.reflect.Field;

//练习反射操作注解
public class Test11 {
    public static void main(String[] args) throws ClassNotFoundException, NoSuchFieldException {
        Class c1 = Class.forName("backend.Java_Demo.reflection.Student2");

        //通过反射获得注解
        Annotation[] annotations = c1.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation);
        }

        //获得注解的value的值
        Tablehuhu tablehuhu = (Tablehuhu) c1.getAnnotation(Tablehuhu.class);
        String value = tablehuhu.value();
        System.out.println(value);

        //获得类指定的注解
        Field field = c1.getDeclaredField("name");
        Fieldhuhu annotation = field.getAnnotation(Fieldhuhu.class);
        System.out.println(annotation.columnName());
        System.out.println(annotation.type());
        System.out.println(annotation.length());
    }
}


@Tablehuhu("db_student")
class Student2 {
    @Fieldhuhu(columnName = "db_id", type = "int" ,length = 10)
    private int id;
    @Fieldhuhu(columnName = "db_age", type = "int" ,length = 10)
    private int age;
    @Fieldhuhu(columnName = "db_name", type = "varchar" ,length = 3)
    private String name;

    public Student2() {

    }

    public Student2(String name, int id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student2{" +
                "id=" + id +
                ", age=" + age +
                ", name='" + name + '\'' +
                '}';
    }
}

//类名的注解
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Tablehuhu{
    String value();
}

//属性的注解
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface Fieldhuhu{
    String columnName();
    String type();
    int length();

}