package backend.Java_Demo.reflection;

//测试类是什么时候被初始化
public class Test05 {
    static {
        System.out.println("main类被加载");
    }

    public static void main(String[] args) throws ClassNotFoundException {
        //1. 主动引用
        //Son son = new Son();

        //反射也会产生主动引用
        //Class.forName("com.reflection.Son");

        //不会产生类的引用的方法
        //System.out.println(Son.b);      //子类不会被加载

        //Son[] array = new Son[5];

        System.out.println(Son.M);      //常量在链接阶段就进入常量池中，调用不需要初始化

    }
}
class Father{
    static int b = 2;
    static {
        System.out.println("父类被加载");
    }
}
class Son extends Father{
    static {
        System.out.println("子类被加载");
        m = 300;
    }
    static int m = 100;
    static final int M = 1;
}
