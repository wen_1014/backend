package backend.Java_Demo.wen.base;

public class Demo02 {
    public static void main(String[] args) {
        //八大数据类型

        //整数
        int num1 = 10;
        byte num2 = 20;
        short num3 = 30;
        long num4 = 30L;        //Long类型要在数字后面加个L

        //小数：浮点数
        float num5 = 50.1F;     //float类型要在数字后面加个F
        double  num6 = 3.14159265358979;

        //字符
        char name = '网';

        //字符串，String不是关键字，类
        String namea = "小网管儿";

        //布尔值：true/false
        boolean flag = true;
        //boolean flag = false;

    }
}
