package backend.Java_Demo.wen.base;

public class Demo07 {
    public static void main(String[] args) {
        //int a,b,c;        尽量不要这样写，为了程序的可读性，尽量每个变量分行定义
        int a = 1;
        int b = 2;
        int c = 3;
        String name = "小网管儿";
        char x = 'X';
        double pi = 3.14;
    }
}
