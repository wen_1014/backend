package backend.Java_Demo.wen.base;

public class Demo05 {
    public static void main(String[] args) {
        int i = 128;
        byte b = (byte)i;       //高转低，需要强制转换        内存溢出      byte类型：-128 - 127
        double c = i;           //低转高，不需要强制转换

        System.out.println(i);
        System.out.println(b);
        System.out.println(c);


        System.out.println("=====================");
        System.out.println((int)23.7);          //23.7为double
        System.out.println((int)-45.89f);       //-45.89为float

        System.out.println("=====================");
        char d = 'a';
        int e = d+1;
        System.out.println(e);
        System.out.println((char)e);
    }
}
