package backend.Java_Demo.wen.base;

public class Demo04 {
    public static void main(String[] args) {

        //float     有限  离散  舍入误差    大约  接近但不等于
        //最好避免完全使用浮点数进行比较
        float f = 0.1f; //0.1
        double d = 1.0/10;  //0.1

        System.out.println(f==d);   //false

        float d1 = 231231231231f;
        float d2 = d1 + 1;

        System.out.println(d1==d2);     //true
        char c1 = 'a';
        char c2 = '网';

        System.out.println(c1);
        System.out.println((int)c1);        //强制转换
        System.out.println(c2);
        System.out.println((int)c2);        //强制转换

        //所有的字符本质还是数字
        //编码    Unicode表：（97 = a    65 = A)  2字节   0 - 65536

        char c3 = '\u0061' ;        //十六进制0061对应字符a
        System.out.println(c3);
    }

    }

