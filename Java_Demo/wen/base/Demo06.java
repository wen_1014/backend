package backend.Java_Demo.wen.base;

public class Demo06 {
    public static void main(String[] args) {
        //操作数比较大的数的时候，注意溢出问题
        //JDK新特性，数字之间可以用下划线分割
        int money = 10_0000_0000;
        int years = 20;
        int total = money*years;    //-1474836480,计算时溢出了
        long total2 = money*years;  //默认是int，转换之前已经溢出了
        long total3 = money*((long)years);  //先把其中一个数转换为long再计算

        System.out.println(total);
        System.out.println(total2);
        System.out.println(total3);

    }
}
