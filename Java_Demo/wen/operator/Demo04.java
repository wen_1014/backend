package backend.Java_Demo.wen.operator;

public class Demo04 {
    public static void main(String[] args) {
        int a = 3;
        //a++   先给b赋值，再自增
        int b = a++;
        //++a   先自增，在给c赋值
        int c = ++a;

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

    }
}
