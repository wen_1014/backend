package backend.Java_Demo.wen.operator;

public class Demo05 {
    public static void main(String[] args) {
        //与（and)    或（or）   非（取反）
        boolean a =true;
        boolean b =false;

        System.out.println("a && b: "+(b&&a));  //逻辑与运算：两个变量都为真，结果才为true
        System.out.println("a || b: "+(b||a));  //逻辑或运算：两个变量有一个为真，结果才为true
        System.out.println("！（a && b）: "+!(b&&a));  //如果为真，则变为假，如果是假则变为真

        //短路运算
        int c = 5;
        boolean d = (c<4)&&(c++<4);     //在运算时，c<4已确定d为false，所以不往下运算，c没有自增
        System.out.println(c);                                //&&相较于&有短路功能
        d = (c<4)&(c++<4);              //&即使c<4已确定为false也会继续向下运算
        System.out.println(d);
        System.out.println(c);

    }
}
