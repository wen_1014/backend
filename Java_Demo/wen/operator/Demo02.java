package backend.Java_Demo.wen.operator;

public class Demo02 {
    public static void main(String[] args) {
        long a =123123123123L;
        int b = 123;
        short c = 10;
        byte d = 8;

        //如果里面有long或double则数据类型为Long或double，按优先级
        System.out.println(a+b+c+d);    //Long
        System.out.println(b+c+d);    //Int
        System.out.println(c+d);    //Int

    }
}
