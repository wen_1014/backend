package backend.Java_Demo.wen.operator;

public class Demo07 {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;

        a+=b;   //a = a+b
        a-=b;   //a = a-b

        //字符串连接符  +   两侧只要出现string类型，都将数转换为string再进行相加
        System.out.println(a+b);
        System.out.println(""+a+b);
        System.out.println(""+(a+b));

    }
}
