package backend.Java_Demo.wen.operator;

public class Demo06 {
    public static void main(String[] args) {
        /*
        A = 0011 1100
        B = 0000 1101

        A&B 0000 1100
        A|B 0011 1101
        A^B 0011 0001       //异或
        ~B  1111 0010

        2*8 = 16  ->  2*2*2*2

       <<       位左移
       >>       位右移

         */
        System.out.println(2<<3);       //2左移3位，等于16
        System.out.println(2>>1);        //2右移1位，等于1
    }
}
