package backend.Java_Demo.wen.struct;
/*for语句格式：
for(初始化;布尔表达式;更新)
循环体;
*/
public class ForDemo01 {
    public static void main(String[] args) {
        int sum = 0;
        for(int i =1;i<=100;i++)
        {
            System.out.println(i);
            sum = sum+i;
        }
        System.out.println(sum);
        System.out.println("for循环结束！");
    }
}
