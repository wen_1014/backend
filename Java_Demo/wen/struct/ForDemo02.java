package backend.Java_Demo.wen.struct;

public class ForDemo02 {
    public static void main(String[] args) {
        //练习1：计算0到100之间的奇数和、偶数和

        int oddSum = 0;     //奇数和
        int evenSum = 0;    //偶数和

        for(int i = 0;i<=100;i++){
            if (i % 2 == 0) {
                evenSum+=i;
            }else {
                oddSum+=i;
            }
        }
        System.out.println("奇数和为"+oddSum);
        System.out.println("偶数和为"+evenSum);
    }
}
