package backend.Java_Demo.wen.struct;

public class ForDemo03 {
    public static void main(String[] args) {
        //练习2：输出1-1000之间能被5整除的数，并且每行输出3个

        for (int i = 1;i <= 1000;i++)
        {
            if(i%5==0){
                System.out.print(i+"\t");
            }
            if (i%15==0){
                System.out.println();
            }
        }
    }
    //println   输出完会换行      :println==printline
    //print   输出完不换行
}
