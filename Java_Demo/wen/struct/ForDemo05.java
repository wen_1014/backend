package backend.Java_Demo.wen.struct;

public class ForDemo05 {
    public static void main(String[] args) {
        //增强for循环

        int[] numbers = {10,20,30,40,50};   //定义了一个数组
        int[][] numbers1 = {{10,20},{30,40},{50,60}};
        for (int i = 0;i<5;i++)
        {
            System.out.println(numbers[i]);
        }

        System.out.println("=========================");

        //遍历数组的元素，与上面的for循环等效
        for (int x:numbers){
            System.out.println(x);
        }


        System.out.println("=========================");
        //遍历多维数组
        for (int[] x:numbers1)
        {
            for (int y:x)
                System.out.println(y);
        }

    }
}
