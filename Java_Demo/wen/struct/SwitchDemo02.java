package backend.Java_Demo.wen.struct;

public class SwitchDemo02 {
    public static void main(String[] args) {
        String name = "txw";
        //JDK7的新特性，表达式结果可以是字符串！
        //字符的本质还是数字

        //反编译   java--class （字节码文件）----反编译（IDEA)
        switch (name){
            case "小网管儿":
                System.out.println("小网管儿");
                break;
            case "网管儿":
                System.out.println("网管儿");
                break;
            case "小网管":
                System.out.println("小网管");
                break;
            default:
                System.out.println("???");
        }
    }
}
