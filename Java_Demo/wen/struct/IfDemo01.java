package backend.Java_Demo.wen.struct;

import java.util.Scanner;

public class IfDemo01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();

        //equals：判断字符串是否相等
        if (s.equals("Hello"))
        {
            System.out.println(s);
        }
        else {
            System.out.println("End");
        }
        scanner.close();

    }
}
