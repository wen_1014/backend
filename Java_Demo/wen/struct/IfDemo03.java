package backend.Java_Demo.wen.struct;

import java.util.Scanner;

public class IfDemo03 {
    public static void main(String[] args) {
        //考试分数90~100为A，80~89为B，70~89为C，60~69为D，60以下为E
        Scanner scanner = new Scanner(System.in);
        /*
        if语句至多有 1 个else语句，else语句在所有的else if语句之后
        if语句可以有若干个else if语句，它们必须在else语句之前
        一旦其中一个else if 语句检测为true，其他的else if 以及else 语句都将跳过执行
         */

        System.out.println("请输入成绩：");
        float score = scanner.nextFloat();
        System.out.println("成绩为"+score);
        if(score>=90){
            System.out.println("成绩等级为A");
            if(score==100)
                System.out.println("恭喜满分！！！");
        }
        else if(score>=80)
            System.out.println("成绩等级为B");
        else if (score>=70)
            System.out.println("成绩等级为C");
        else if (score>=60)
            System.out.println("成绩等级为D");
        else System.out.println("成绩等级为E");

        scanner.close();
    }
}
