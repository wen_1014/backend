package backend.Java_Demo.wen.struct;

import java.util.Scanner;

//分析分数的等级
public class SwitchDemo01 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入成绩：");
        float score = scanner.nextFloat();
        char grade;
        if(score>=90){
            grade = 'A';
        }
        else if (score>=80){
            grade = 'B';
        }
        else if (score>=70){
            grade = 'C';
        }
        else if (score>=60){
            grade = 'D';
        }
        else
            grade = 'E';
        System.out.println("成绩为："+score);
        System.out.println("等级为："+grade);
        switch (grade){
            case 'A':
                System.out.println("优秀！！！！");
                break;
            case 'B':
                System.out.println("良好！！！");
                break;
            case 'C':
                System.out.println("一般！！");
                break;
            case 'D':
                System.out.println("及格！");
                break;
            case 'E':
                System.out.println("请您立即联系教务处办理退学手续，谢谢！");
                break;
            default:
                System.out.println("未知等级");
        }
    }
}
