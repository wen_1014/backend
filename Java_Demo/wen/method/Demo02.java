package backend.Java_Demo.wen.method;

public class Demo02 {
    public static void main(String[] args) {
    double max = max(10,20);       //方法重载：当返回值为double则调用返回值为double方法，当返回值为int则调用返回值为int的方法
        System.out.println(max);
    }

    //比大小
    public static double max(double num1,double num2){
        double result = 0;

        if(num1==num2){
            System.out.println("num1==num2");
            return 0;   //终止方法
        }

        if (num1 > num2){
            result = num1;
        }else {
            result = num2;
        }
        return result;
    }

        //比大小
    public static int max(int num1,int num2){
        int result = 0;

        if(num1==num2){
            System.out.println("num1==num2");
            return 0;   //终止方法
        }

        if (num1 > num2){
            result = num1;
        }else {
            result = num2;
        }
        return result;
    }
}
