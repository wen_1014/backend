package backend.Java_Demo.wen.method;

import java.util.Scanner;

public class DemoPractice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a;
        double b;
        System.out.println("请输入第1个数字:");
        a=scanner.nextDouble();
        System.out.println("请输入第2个数字");
        b=scanner.nextDouble();
        System.out.println(max1(a,b));
        System.out.println(max2(a,b));
        System.out.println(max3(a,b));
    }
    //比大小
    public static double max1(double num1,double num2){
        int result = 0;

        if (num1 == num2){
            System.out.println("num1 = num2");
            return num1;       //终止方法
        }
        if(num1>num2){

        return num1;
        }else {
            return num2;
        }
    }
    //下面给出两种更简单的实现比较大小

    //三目运算符
    public static double max2(double num1,double num2){
        return num1>=num2?num1:num2;
    }
    //调用Math类中的max方法,返回两个参数中的最大值
    public static double max3(double num1,double num2){
        return Math.max(num1, num2);
    }
}