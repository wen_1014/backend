package backend.Java_Demo.oop.demo06;

public class Application {
    public static void main(String[] args) {
        //静态的方法和非静态的方法区别很大
        //非静态才能重写
        //方法的调用只和左边，定义的数据类型有关
        A a = new A();
        a.test();

        //父类的引用指向了子类
        B b = new A();      //子类重写了父类
        b.test();
    }
}
