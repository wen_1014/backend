package backend.Java_Demo.oop.demo12;

public class Outer {
    private int id = 10;
    public void out(){
        System.out.println("这是外部类的方法");
    }

    //局部内部类：在方法中的类
    public void method(){
        class Inner1{
            public void in(){

            }
        }
    }

    //一个java类中可以有多个class，但只能有一个public class
    class  Inner{
        public void in(){
            System.out.println("这是内部类的方法");
        }

        //获得外部类的私有属性
        public void getID(){
            System.out.println(id);
        }
    }

}
