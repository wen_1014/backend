package backend.Java_Demo.oop.demo12;

public class Test {
    public static void main(String[] args) {
        //匿名内部类： 没有名字初始化类,不用将实例保存到变量中
        new Apple().eat();

        new UserService1(){
            @Override
            public void hello() {
            }
        };
    }
}

class Apple{
    public void eat(){
        System.out.println("1");
    }
}

interface UserService1{
    void hello();
}