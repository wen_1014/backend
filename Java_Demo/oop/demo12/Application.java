package backend.Java_Demo.oop.demo12;

public class Application {
    public static void main(String[] args) {
        Outer outer = new Outer();
        //通过这个外部类去实例化内部类
        Outer.Inner inner = outer.new Inner();
        inner.in();
        inner.getID();
    }
}
