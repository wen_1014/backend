package backend.Java_Demo.oop.demo09;

//static
public class Student {
    private static int age;
    private double socre;

    public static void main(String[] args) {
        Student s1 = new Student();
        System.out.println(Student.age);
        // System.out.println(Student.score);    //报错
        System.out.println(s1.age);
        System.out.println(s1.socre);


    }

    public void run() {
    go();
    }

    public static void go() {
   // run();    //报错，静态方法需要使用非静态方法之前应先实例化对象,，因为在类加载时，只加载了静态方法，没有加载非静态方法
        Student student = new Student();
        student.run();
    }
}
//static和类一起加载