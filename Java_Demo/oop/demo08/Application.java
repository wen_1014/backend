package backend.Java_Demo.oop.demo08;

public class Application {
    public static void main(String[] args) {
        //类型之间的转换：父  子

        //高--->低
        Person obj = new Student();

        //student将这个对象转换为Student类型，我们就可以使用Student类型的方法了
        //1
        Student student = (Student) obj;    //强转
        student.go();
        student.run();
        //2.
        ((Student)obj).go();

        //子类转换为父类，可能丢失自己本来的一些独特的方法
        Student student1 = new Student();
        student.go();
        Person person = student1;    //自动转
        //person.go();      //编译报错
    }

}


/*
    public static void main(String[] args) {
        //Object > String
        //Object > Person > Student
        //Object > Person > Teacher

        Object object = new Student();
        System.out.println(object instanceof Student);  //ture
        System.out.println(object instanceof Person);   //ture
        System.out.println(object instanceof Object);   //ture
        System.out.println(object instanceof Teacher);  //false
        System.out.println(object instanceof String);   //false
        System.out.println("===================================");
        Person person = new Student();
        System.out.println(person instanceof Student);  //true
        System.out.println(person instanceof Person);   //true
        System.out.println(person instanceof Object);   //true
        System.out.println(person instanceof Teacher);  //false
        //  System.out.println(person instanceof String);   //编译就报错
        System.out.println("===================================");
        Student student = new Student();
        System.out.println(student instanceof Student);  //true
        System.out.println(student instanceof Person);   //true
        System.out.println(student instanceof Object);   //true
        //  System.out.println(student instanceof Teacher);  //编译即报错
        //  System.out.println(student instanceof String);   //编译即报错

        //编译是否通过，看对象类型和目标是否存在父子关系
        //ture还是false主要看对象引用类型和目标是否存在父子关系
    }
 */