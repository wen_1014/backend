package backend.Java_Demo.oop.demo07;

public class Application {
    public static void main(String[] args) {

        //一个对象的是实际类型是确定的
        //new Student();
        //new Person();

        //可以指向的引用类型不确定： 父类的引用指向子类
        Student s1 = new Student();
        Person s2 = new Student();
        Object s3 = new Student();

        //对象能执行哪些方法，主要看对象左边的类型，和右边的关系不打
        s2.run();   //子类重写了父类的方法，执行子类的方法
        //s2.eat();   //报错，Person类型没有这个方法，子类重写父类的方法，但父类不会得到子类特有的方法
        ((Student) s2).eat();       //强制转换，将对象类型强制转换为student，高转低，强转
        s1.run();
    }
}
