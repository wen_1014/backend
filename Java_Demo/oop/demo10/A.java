package backend.Java_Demo.oop.demo10;

//抽象类的所有方法，继承了
public class A extends Action {
    //抽象类子类必须重写父类的抽象方法，否则会报错
    @Override
    public void doSomething() {

    }
}
