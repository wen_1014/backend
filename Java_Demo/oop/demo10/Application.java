package backend.Java_Demo.oop.demo10;

public class Application {
    public static void main(String[] args) {
    //  new Action(); //报错，抽象类不能被实例化，只能通过子类去实现（new子类对象）
    }
}
