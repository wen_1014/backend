package backend.Java_Demo.oop.demo02;

public class Application {
    public static void main(String[] args) {

        //new 实例化了一个对象
        //无参构造
        Person person1 = new Person();
        Person person = new Person("hu_SAMA");

        System.out.println(person1.name);
        System.out.println(person.name);    //hu_SAMA
    }
}

