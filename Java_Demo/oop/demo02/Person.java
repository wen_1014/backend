package backend.Java_Demo.oop.demo02;

//java---> class
public class Person {
    //alt + insert 快速生成构造器

   //一个类即使什么都不写，它也会存在一个方法
    //显示的定义构造器

    String name;

    //实例化初始值
    //1、使用new关键字，本质是在调用构造器
    //2、构造器一般用来初始化值

    //无参构造
    public Person(){
        this.name = "hu_SAMA";
    }

    //有参构造:一旦定义了有参构造，无参就必须显示定义（无有参构造时，会自动生成一个无参构造）
    public Person(String name){
        this.name = name;
    }
}

/*
    构造器：
        1、和类名相同
        2、没有返回值
    作用：
        1、new 本质在调用构造方法
        2、初始化对象的值
    注意点：
        1、定义有参构造后，如果想使用无参构造，显得地定义一个无参构造
    构造器快捷键：
        alt + insert
 */
