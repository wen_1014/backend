package backend.Java_Demo.oop.demo02;

//学生类
public class Student {
    //属性：字段
    String name;        //初始默认为null
    int age;            //初始默认为0

    //方法
    public void study(){
        System.out.println("wenSAMA_糊");
    }
}

/*public static void main(String[] args) {

        //类：抽象的，实例化
        //类实例化后会返回一个自己的对象
        //student对象就是一个Student类的具体事例

        Student huhuhu = new Student();
        Student huhuSAMA = new Student();

        huhuhu.name = "糊SAMA";
        huhuhu.age = 118;

        huhuSAMA.name = "米古月米古月";
        huhuSAMA.age = 119;

        System.out.println(huhuhu.name);
        System.out.println(huhuhu.age);

        System.out.println(huhuSAMA.name);
        System.out.println(huhuSAMA.age);
    }

 */