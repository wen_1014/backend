package backend.Java_Demo.oop.demo05;

//子（派生）类
public class Student extends Person {
    public Student(){
        //隐藏代码：调用了父亲的无参构造
        super("name");  //调用父亲的构造器，必须在子类构造器的第一行
        System.out.println("Student的无参构造执行了");
    }
    private String name = "wen_SAMA";


    public void print(){
        System.out.println("Student");
    }

    public void test1(){
        print();        //Student
        this.print();   //Student
        super.print();  //Person
    }

}
