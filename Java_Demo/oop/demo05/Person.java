package backend.Java_Demo.oop.demo05;

//在Java中，所有的类，都默认直接或间接地继承Object（里面有一些基本的方法）
//父（基）类
//子类继承了父类，就会拥有父类的全部方法
public class Person {
    public Person(String name){
        System.out.println("Person");
    }
    protected String name = "hu_SAMA";

    public void print(){
        System.out.println("Person");
    }
}
