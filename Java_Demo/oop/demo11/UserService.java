package backend.Java_Demo.oop.demo11;

public interface UserService {
    //常量 public abstract final
    int age = 99;

    //接口中的所有定义都是抽象的  public abstract
    void add(String name);
    void delete(String name);
    void update(String name);
    void query(String name);

}
