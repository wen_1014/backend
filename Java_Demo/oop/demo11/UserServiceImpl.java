package backend.Java_Demo.oop.demo11;

//通过类实现接口
//实现接口的类，就需要重写接口的所有方法

//多继承，利用接口实现多继承

public class UserServiceImpl implements UserService,TimeService {

    @Override
    public void add(String name) {

    }

    @Override
    public void delete(String name) {

    }

    @Override
    public void update(String name) {

    }

    @Override
    public void query(String name) {

    }

    @Override
    public void timer() {

    }
}
