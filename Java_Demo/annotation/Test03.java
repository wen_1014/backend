package backend.Java_Demo.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class Test03 {
    //注解可以显示赋值， 如果没有默认值，就必须给注解赋值
    @MyAnnotation2(name = "hu_sama",SAMAname = {"huhu_SAMA"})
    public void test1(){}

    @MyAnnotation3("当注解中只有一个值时，默认给该参数赋值")
    public void test2(){}
}

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@interface MyAnnotation2{
    //注解的参数 ： 参数类型 + 参数名();
    String name()  default "";
    int age() default 18;
    int id() default -1;

    String[] SAMAname();
}

@interface MyAnnotation3{
    String value();
}