package backend.Java_Demo.annotation;

@SuppressWarnings("all")
public class Test01 extends Object {
    @Override   //重写的注解
    public String toString() {
        return super.toString();
    }

    //Deprecated   不推荐程序员使用，但是可以使用，或者存在更好的方式
    @Deprecated
    public static void test(){
        System.out.println("Deprecated");
    }

    public static void main(String[] args) {
        test();
    }

}
