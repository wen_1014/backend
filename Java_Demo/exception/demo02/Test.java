package backend.Java_Demo.exception.demo02;

public class Test {
    //可能会存在异常的方法

    static void test(int a) throws MyException{ //在方法上用throws往更高的地方抛出
        //个人理解：在方法内用throw抛出后异常是在方法内的，只能在方法内捕获
        //如要在方法外捕获必须在方法上加上throws往更高处抛出，抛出后可在整个类里被捕获

        System.out.println("传递的参数为："+a);
        if(a>10){
            throw new MyException(a);    //抛出
        }

        System.out.println("OK");
    }

    public static void main(String[] args) {
        try{
            test(11);
        }catch (MyException e){
            //可以在catch里增加一些处理异常的代码块
            System.out.println("MyException=>" +e); //这里的e是MyException中的toString方法
        }
    }
}
