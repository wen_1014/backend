package backend.Java_Demo.exception.demo01;

public class Test {
    public static void main(String[] args) {
        try {
            new Test().test(1,0);
        } catch (ArithmeticException e) {
            e.printStackTrace();
        }


        /*
        int a = 1;
        int b = 0;

        //假设要捕获多个异常：从小到大
        try {
            //try监控区域

            System.out.println(a / b);
        } catch (ArithmeticException e) {         //catch中为想要捕获的异常类型，当捕获到的异常相同时，执行体内的功能
            System.out.println("程序出现异常，变量b不能为0");
        } catch (Exception e) {
            System.out.println("Exception");
        } catch (Throwable t) {
            System.out.println("Throwable");
        } finally {
            //处理善后工作
            System.out.println("finally");
        }
        //finally 可以不要


         */
/*
        try {
            //try监控区域
            new Test().a();
        }catch (ArithmeticException e){
            System.out.println("程序出现异常，变量b不能为0");
        }finally {
            //处理善后工作
            System.out.println("finally");
        }
    }




    public void a(){
        b();
    }
    public void b(){
        a();
    }


 */
    }

    //假设在方法中处理不了异常，方法上抛异常
    public void test(int a,int b) throws ArithmeticException{
        if (b == 0) {//主动抛出异常    throw   throws
            throw new ArithmeticException();  //主动抛出异常
        }
        System.out.println(a/b);
    }
}
